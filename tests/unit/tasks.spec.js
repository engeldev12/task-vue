import Vue from 'vue';

import TaskComponentFactory  from '../fakes/taskFactory';


describe('Task.vue', () => {
	
	let wrapper = null;
	
	describe('cuando una tarea esté pendiente', () => {
		
		it('su status debe ser Pendiente', () => {
			
			let task = { isDone: false }

			wrapper = TaskComponentFactory({task: task});

			expect(wrapper.vm.status).toEqual('Pendiente');

		});

	});

	describe('cuando una tarea este terminada', () => {
		
		it('su status será Terminada', () => {
			
			let task = { isDone: true }

			wrapper = TaskComponentFactory({task: task});

			expect(wrapper.vm.status).toEqual('Terminada');

		})

	})

	describe('cuando marque una tarea pendiente como terminada', () => {
	
		it('debe decir "La tarea ha sido terminada." ', () => {

			let task = { isDone: false }

			wrapper = TaskComponentFactory({task: task});

			wrapper.find('button').trigger('click');

			expect(wrapper.vm.message).toEqual("La tarea ha sido terminada.");

		})
	
	})

	describe('cuando intente marcar una tarea terminada', () => {
	
		it('debe decir "Esta tarea ya ha sido terminada." ', () => {

			let task = { isDone: true }

			wrapper = TaskComponentFactory({task: task});

			wrapper.find('button').trigger('click');

			expect(wrapper.vm.message).toEqual("Esta tarea ya ha sido terminada.");

		})
	
	})

})