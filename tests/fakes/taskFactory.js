import { shallowMount } from '@vue/test-utils';
import Task from '@/components/Task.vue';


const TaskComponentFactory = (propsData) =>  {
	return shallowMount(Task, {
			propsData: {
				...propsData
			}
		}
	)
}

export default TaskComponentFactory